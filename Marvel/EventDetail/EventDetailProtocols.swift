import Foundation


protocol EventDetailRouterProtocol: AnyObject {
    
}

protocol EventDetailPresenterProtocol: AnyObject {
    
}


protocol EventDetailInteractorProtocol: AnyObject {
    var presenter: EventDetailPresenterProtocol?  { get set }
}


protocol EventDetailViewProtocol: AnyObject {
    var presenter: EventDetailPresenterProtocol?  { get set }
}
