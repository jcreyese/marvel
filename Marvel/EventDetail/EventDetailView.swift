import UIKit


class EventDetailView: UIViewController, EventDetailViewProtocol {
    
	
    var presenter: EventDetailPresenterProtocol?
    
    @IBOutlet weak var eventDetailTableView: UITableView!
    @IBOutlet weak var containerTableView: UIView!
    
    private let event: Event
    
    
    init(event: Event) {
        self.event = event
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.setUpTableView()
        self.registerCell(tableView: eventDetailTableView)
    }
    
    func setUpTableView() {
        self.eventDetailTableView.delegate = self
        self.eventDetailTableView.dataSource = self
        self.containerTableView.layer.cornerRadius = 4
        self.containerTableView.clipsToBounds = true
        
    }
    
    func registerCell(tableView: UITableView) {
        let identifierImage = "EventHeaderTableViewCell"
        tableView.register(UINib(nibName: identifierImage, bundle: nil), forCellReuseIdentifier: identifierImage)
        let identifierComic = "EventComicTableViewCell"
        tableView.register(UINib(nibName: identifierComic, bundle: nil), forCellReuseIdentifier: identifierComic)
    }
    
    @IBAction func closeModalButtonAction(_ sender: UIButton) {
        if (self.presentingViewController != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension EventDetailView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let items = event.comics?.items?.count ?? 0
        return items + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "EventHeaderTableViewCell") as? EventHeaderTableViewCell {
                if let path = event.thumbnail?.path, let ext = event.thumbnail?.ext {
                    let link = path.replacingOccurrences(of: "http://", with: "https://")
                    let urlString = "\(link).\(ext)"
                    cell.eventImage.kf.indicatorType = .activity
                    cell.eventImage.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.5))])
                }
                
                cell.titleEventLabel.text = event.title ?? ""
                cell.dateEventLabel.text = event.start
                cell.subtitleLabel.text = "COMICS TO DISCUSS"
                
                return cell
            }
        default:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "EventComicTableViewCell") as? EventComicTableViewCell {
                cell.titleComicLabel.text = event.comics?.items?[indexPath.row - 1].name ?? ""
                cell.dateComicLabel.text = event.comics?.items?[indexPath.row - 1].resourceURI ?? ""
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 160
        default:
            return 88
        }
    }
    
}
