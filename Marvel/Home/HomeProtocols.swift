import Foundation


protocol HomeRouterProtocol: AnyObject {
    func goBackToAuth()
}

protocol HomePresenterProtocol: AnyObject {
    func onCloseSessionButtonAction()
}


protocol HomeInteractorProtocol: AnyObject {
    var presenter: HomePresenterProtocol?  { get set }
}


protocol HomeViewProtocol: AnyObject {
    var presenter: HomePresenterProtocol?  { get set }
    
    func showToast(message: String)
}
