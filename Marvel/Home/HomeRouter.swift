import UIKit


class HomeRouter: HomeRouterProtocol {
    
    
    weak var viewController: UIViewController?
    
    
    func goBackToAuth() {
        self.viewController?.dismiss(animated: false)
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
}
