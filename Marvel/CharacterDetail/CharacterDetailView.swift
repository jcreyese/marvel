import UIKit
import Kingfisher


class CharacterDetailView: UIViewController, CharacterDetailViewProtocol {
    
	
    var presenter: CharacterDetailPresenterProtocol?
    
    @IBOutlet weak var characterDetailTableView: UITableView!
    
    private let character: Character
    
    
    init(character: Character) {
        self.character = character
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = character.name
        self.setUpTableView()
        self.registerCell(tableView: characterDetailTableView)
        self.setUpNavBar()
    }
    
    func setUpTableView() {
        characterDetailTableView.delegate = self
        characterDetailTableView.dataSource = self
    }
    
    func registerCell(tableView: UITableView) {
        let identifierImage = "CharacterImageTableViewCell"
        tableView.register(UINib(nibName: identifierImage, bundle: nil), forCellReuseIdentifier: identifierImage)
        let identifierDescription = "CharacterDescriptionTableViewCell"
        tableView.register(UINib(nibName: identifierDescription, bundle: nil), forCellReuseIdentifier: identifierDescription)
        let identifierComic = "CharacterComicTableViewCell"
        tableView.register(UINib(nibName: identifierComic, bundle: nil), forCellReuseIdentifier: identifierComic)
    }
    
    func setUpNavBar() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension CharacterDetailView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let items = character.comics?.items?.count ?? 0
        return items + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterImageTableViewCell") as? CharacterImageTableViewCell {
                if let path = character.thumbnail?.path, let ext = character.thumbnail?.ext {
                    let link = path.replacingOccurrences(of: "http://", with: "https://")
                    let urlString = "\(link).\(ext)"
                    cell.characterImage.kf.indicatorType = .activity
                    cell.characterImage.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.5))])
                }
                
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterDescriptionTableViewCell") as? CharacterDescriptionTableViewCell {
                cell.descriptionCharacterLabel.text = character.description ?? ""
                cell.subtitleLabel.text = "APPEARS IN THESE COMICS"
                
                return cell
            }
        default:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterComicTableViewCell") as? CharacterComicTableViewCell {
                cell.titleComicLabel.text = character.comics?.items?[indexPath.row - 2].name ?? ""
                cell.dateComicLabel.text = character.comics?.items?[indexPath.row - 2].resourceURI ?? ""
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return UIScreen.main.bounds.width
        case 1:
            return 180
        default:
            return 88
        }
    }
}
