import Foundation


protocol CharacterDetailRouterProtocol: AnyObject {
    
}

protocol CharacterDetailPresenterProtocol: AnyObject {
    
}


protocol CharacterDetailInteractorProtocol: AnyObject {
    var presenter: CharacterDetailPresenterProtocol?  { get set }
}


protocol CharacterDetailViewProtocol: AnyObject {
    var presenter: CharacterDetailPresenterProtocol?  { get set }
}
