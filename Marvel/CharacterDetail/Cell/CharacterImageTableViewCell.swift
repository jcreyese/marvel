import UIKit

class CharacterImageTableViewCell: UITableViewCell {

    
    @IBOutlet weak var characterImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
