import UIKit


class EventsInteractor: EventsInteractorProtocol {
    
    
    weak var presenter: EventsPresenterProtocol?
    
    
    func invokeEventsServiceWith(param: String) {
        let service = APIRestService(urlServer: "https://gateway.marvel.com:443/v1/public/events?")
        let auth = "ts=1&apikey=4cb38e9a77dd01e2463bff9981683b93&hash=b6dade92c534cbc76dc69cc6004ed9b2"
        service.getEventsWith(param: param, auth: auth)
        service.completionHandlerEvent { [weak self] events, status, message in
            if status {
                guard let self = self else { return }
                guard let _events = events else { return }
                self.presenter?.getEventListSuccess(result: _events)
            } else {
                self?.presenter?.getEventListFailure()
            }
        }
    }
    
}
