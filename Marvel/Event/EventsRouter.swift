import UIKit


class EventsRouter: EventsRouterProtocol {
    
    
    weak var viewController: UIViewController?
    
    
    func goToEventDetail(selection: Event) {
        let modalEventDetail = EventDetailModule.build(event: selection)
        modalEventDetail.modalPresentationStyle = .fullScreen
        let viewModal = UINavigationController(rootViewController: modalEventDetail)
        self.viewController?.present(viewModal, animated: true)
    }
    
}
