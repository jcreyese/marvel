import UIKit


class EventsModule {
    
    static func build() -> UIViewController {        
        let view = EventsView()
        let interactor = EventsInteractor()
        let router = EventsRouter()
        let presenter = EventsPresenter()
        
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        router.viewController = view
        
        return view
    }
    
}
