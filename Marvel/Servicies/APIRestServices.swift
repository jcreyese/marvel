import Foundation
import Alamofire


class APIRestService {
    
    
    fileprivate var urlServer = ""
    typealias charactersCallBack = (_ character: CharacterDataWrapper?, _ status: Bool, _ message: String) -> Void
    var characterCallBack: charactersCallBack?
    typealias eventsCallBack = (_ events: EventDataWrapper?, _ status: Bool, _ message: String) -> Void
    var eventCallBack: eventsCallBack?
    
    
    // Init
    init(urlServer: String) {
        self.urlServer = urlServer
    }
    
    
    // GetCharactersService
    func getCharactersWith(param: String, auth: String) {
        let url = "\(self.urlServer)\(param)\(auth)"
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response { (responseData) in
            guard let data = responseData.data else {
                self.characterCallBack?(nil, false, "")
                return
            }
            do {
                let characters = try JSONDecoder().decode(CharacterDataWrapper.self, from: data)
                self.characterCallBack?(characters, true, "")
                debugPrint("\n👨‍💻 NETWORK RESPONSE FOR DEBUG -> \(characters)")
            } catch {
                self.characterCallBack?(nil, false, error.localizedDescription)
                debugPrint("\n👨‍💻 NETWORK RESPONSE ERROR FOR DEBUG -> \(error.localizedDescription)")
            }
        }
    }
    
    func completionHandlerCharacter(callBack: @escaping charactersCallBack) {
        self.characterCallBack = callBack
    }
    
    
    // GetEventsService
    func getEventsWith(param: String, auth: String) {
        let url = "\(self.urlServer)\(param)\(auth)"
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response { (responseData) in
            guard let data = responseData.data else {
                self.eventCallBack?(nil, false, "")
                return
            }
            do {
                let events = try JSONDecoder().decode(EventDataWrapper.self, from: data)
                self.eventCallBack?(events, true, "")
                debugPrint("\n👨‍💻 NETWORK RESPONSE FOR DEBUG -> \(events)")
            } catch {
                self.eventCallBack?(nil, false, error.localizedDescription)
                debugPrint("\n👨‍💻 NETWORK RESPONSE ERROR FOR DEBUG -> \(error.localizedDescription)")
            }
        }
    }
    
    func completionHandlerEvent(callBack: @escaping eventsCallBack) {
        self.eventCallBack = callBack
    }
    
}
