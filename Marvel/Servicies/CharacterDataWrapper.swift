import Foundation


struct CharacterDataWrapper: Codable {
    var code: Int?
    var status: String?
//    var copyright: String?
//    var attributionText: String?
//    var attributionHTML: String?
    var data: CharacterDataContainer?
//    var etag: String?
}

struct CharacterDataContainer: Codable {
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    var results: [Character]?
}

struct Character: Codable {
//    var id: Int?
    var name: String?
    var description: String?
//    var modified: String?
//    var resourceURI: String?
//    var urls: [UrlCharacter]?
    var thumbnail: ImageCharacter?
    var comics: ComicListCharacter?
//    var stories: StoryListCharacter?
//    var events: EventListCharacter?
//    var series: SeriesListCharacter?
}

//struct UrlCharacter: Codable {
//    var type: String?
//    var url: String?
//}

struct ImageCharacter: Codable {
    var path: String?
    var ext: String?

    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}

struct ComicListCharacter: Codable {
    var available: Int?
    var returned: Int?
    var collectionURI: String?
    var items: [ComicSummaryCharacter]?
}

struct ComicSummaryCharacter: Codable {
    var resourceURI: String?
    var name: String?
}

//struct StoryListCharacter: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [StorySummaryCharacter]?
//}

//struct StorySummaryCharacter: Codable {
//    var resourceURI: String?
//    var name: String?
//    var type: String?
//}

//struct EventListCharacter: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [EventSummaryCharacter]?
//}

//struct EventSummaryCharacter: Codable {
//    var resourceURI: String?
//    var name: String?
//}

//struct SeriesListCharacter: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [SeriesSummaryCharacter]?
//}

//struct SeriesSummaryCharacter: Codable {
//    var resourceURI: String?
//    var name: String?
//}
