import UIKit
import Alamofire
import Kingfisher


class CharactersPresenter: CharactersPresenterProtocol {
    
    
    weak var view: CharactersViewProtocol?
    var interactor: CharactersInteractorProtocol?
    var router: CharactersRouterProtocol?
    
    var charactersList: [Character] = []
    var charactersTableView: UITableView = UITableView()
    var offset: Int = 0
    var serviceAvailable: Bool = true
    
    
    func numberOfRowsInSection() -> Int {
        return charactersList.count
    }
    
    func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell") as! CharacterTableViewCell
        let character = charactersList[indexPath.row]
        cell.cellView.layer.cornerRadius = 4
        cell.cellView.clipsToBounds = true
        
        if let path = character.thumbnail?.path, let ext = character.thumbnail?.ext {
            let link = path.replacingOccurrences(of: "http://", with: "https://")
            let urlString = "\(link).\(ext)"
            cell.characterImage.kf.indicatorType = .activity
            cell.characterImage.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.5))])
        }
        
        cell.nameCharacterLabel.text = character.name
        cell.descriptionCharacterLabel.text = character.description
        
        return cell
    }
    
    func didSelected(indexPath: IndexPath) {
        let character = charactersList[indexPath.row]
        router?.goToCharacterDetail(selection: character)
    }
    
    func getTableView(tableView: UITableView) {
        self.charactersTableView = tableView
    }
    
    // Service characters
    func getCharacterList() {
        self.view?.showLoading()
        self.interactor?.invokeCharactersServiceWith(param: "limit=15&offset=\(offset)&")
    }
    
    func getCharacterListSuccess(result: CharacterDataWrapper) {
        if let characters = result.data?.results {
            self.charactersList.append(contentsOf: characters)
            self.view?.reloadDataTableView()
            self.offset += 15
            self.serviceAvailable = true
            self.view?.hideLoading()
            self.charactersTableView.tableFooterView = nil
        }
    }
    
    func getCharacterListFailure() {
        self.serviceAvailable = true
        self.view?.hideLoading()
        self.charactersTableView.tableFooterView = nil
    }
    
    
    // Reload TableView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        let contentSizeHeight = self.charactersTableView.contentSize.height
        let scrollHeight = scrollView.frame.size.height
        
        if position > (contentSizeHeight - scrollHeight + 75) && serviceAvailable {
            self.serviceAvailable = false
            self.charactersTableView.tableFooterView = createSpinnerFooter()
            self.interactor?.invokeCharactersServiceWith(param: "limit=15&offset=\(offset)&")
        }
    }
    
    func createSpinnerFooter() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 100.0))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        
        return footerView
    }
}
