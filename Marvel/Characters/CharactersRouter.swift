import UIKit


class CharactersRouter: CharactersRouterProtocol {
    
    
    weak var viewController: UIViewController?
    
    
    func goToCharacterDetail(selection: Character) {
        let characterDetail = CharacterDetailModule.build(character: selection)
        characterDetail.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.pushViewController(characterDetail, animated: true)
    }
}
