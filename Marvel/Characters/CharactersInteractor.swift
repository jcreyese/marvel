import UIKit


class CharactersInteractor: CharactersInteractorProtocol {
    
    
    weak var presenter: CharactersPresenterProtocol?
    
    
    func invokeCharactersServiceWith(param: String) {
        let service = APIRestService(urlServer: "https://gateway.marvel.com:443/v1/public/characters?")
        let auth = "ts=1&apikey=4cb38e9a77dd01e2463bff9981683b93&hash=b6dade92c534cbc76dc69cc6004ed9b2"
        service.getCharactersWith(param: param, auth: auth)
        service.completionHandlerCharacter { [weak self] characters, status, message in
            if status {
                guard let self = self else { return }
                guard let _characters = characters else { return }
                self.presenter?.getCharacterListSuccess(result: _characters)
            } else {
                self?.presenter?.getCharacterListFailure()
            }
        }
    }
    
}
