import UIKit


class CharactersModule {
    
    static func build() -> UIViewController {        
        let view = CharactersView()
        let interactor = CharactersInteractor()
        let router = CharactersRouter()
        let presenter = CharactersPresenter()
        
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        router.viewController = view
        
        return view
    }
    
}
