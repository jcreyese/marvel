import Foundation
import UIKit


protocol CharactersRouterProtocol: AnyObject {
    func goToCharacterDetail(selection: Character)
}

protocol CharactersPresenterProtocol: AnyObject {
    func numberOfRowsInSection() -> Int
    func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell
    func didSelected(indexPath: IndexPath)
    func getTableView(tableView: UITableView)
    func getCharacterList()
    func getCharacterListSuccess(result: CharacterDataWrapper)
    func getCharacterListFailure()
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}


protocol CharactersInteractorProtocol: AnyObject {
    var presenter: CharactersPresenterProtocol?  { get set }
    
    func invokeCharactersServiceWith(param: String)
}


protocol CharactersViewProtocol: AnyObject {
    var presenter: CharactersPresenterProtocol?  { get set }
    
    func reloadDataTableView()
    func showLoading()
    func hideLoading()
}
