import UIKit


class AuthRouter: AuthRouterProtocol {
    
    
    weak var viewController: UIViewController?
    
    
    func goToHomeView(email: String, provider: ProviderType, animated: Bool) {
        let home = HomeModule.build(email: email, provider: provider)
        self.viewController?.navigationController?.pushViewController(home, animated: animated)
    }
}
