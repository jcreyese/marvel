import UIKit
import FirebaseAuth


class AuthPresenter: AuthPresenterProtocol {
    
    
    weak var view: AuthViewProtocol?
    var interactor: AuthInteractorProtocol?
    var router: AuthRouterProtocol?
    
    
    func onPressedSignUpButton(email: String, password: String, provider: ProviderType) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if error == nil {
                self.goToHomeView(email: email, provider: provider, animated: true)
            } else {
                if let message = error?.localizedDescription {
                    self.view?.showToast(message: message)
                }
            }
        }
        
//        self.router?.goToHomeView(email: email, provider: provider, animated: true) // Acceso directo
    }
    
    func onPressedLogInButton(email: String, password: String, provider: ProviderType) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if error == nil {
                self.goToHomeView(email: email, provider: provider, animated: true)
            } else {
                if let message = error?.localizedDescription {
                    self.view?.showToast(message: message)
                }
            }
        }
        
//        self.router?.goToHomeView(email: email, provider: provider, animated: true) // Acceso directo
    }
    
    func isAuth() {
        let defaults = UserDefaults.standard
        if let email = defaults.value(forKey: "email") as? String, let provider = defaults.value(forKey: "provider") as? String {
            self.goToHomeView(email: email, provider: ProviderType.init(rawValue: provider)!, animated: false)
        }
    }
    
    func goToHomeView(email: String, provider: ProviderType, animated: Bool) {
        self.router?.goToHomeView(email: email, provider: provider, animated: animated)
    }
    
}
